/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Pieter Moens
 * Created: Jul 8, 2018
 */

CREATE TABLE IF NOT EXISTS `roles` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    PRIMARY KEY (id)    
) ENGINE = InnoDB;

INSERT IGNORE INTO `roles` (name) VALUES ('ADMIN');
INSERT IGNORE INTO `roles` (name) VALUES ('USER');

CREATE TABLE IF NOT EXISTS `users` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL UNIQUE,
    email VARCHAR(255) NOT NULL UNIQUE,
    hashedPassword VARCHAR(64) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `users_roles` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    user_id INT(11) NOT NULL,
    role_id INT(11) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (role_id) REFERENCES roles (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `suppliers` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    street VARCHAR(255),
    number INT(7),
    postalCode VARCHAR(32),
    city VARCHAR(255),
    country VARCHAR(255),
    phone VARCHAR(32),
    mobile VARCHAR(32),
    fax VARCHAR(32),
    email VARCHAR(255),
    note TEXT,
    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `clients` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    street VARCHAR(255),
    number INT(7),
    postalCode VARCHAR(32),
    city VARCHAR(255),
    country VARCHAR(255),
    phone VARCHAR(32),
    mobile VARCHAR(32),
    fax VARCHAR(32),
    email VARCHAR(255),
    note TEXT,
    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `categories` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    parent_id INT(11),
    PRIMARY KEY (id),
    FOREIGN KEY (parent_id) REFERENCES categories (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `articles` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    code VARCHAR(255) NOT NULL UNIQUE,
    description VARCHAR(255),
    unitPrice DECIMAL DEFAULT 0.0,
    orderNumber VARCHAR(255),
    consignment BOOLEAN,
    user_id INT(11),
    supplier_id INT(11),
    category_id INT(11),
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (supplier_id) REFERENCES suppliers (id),
    FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `rolls` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    date DATE NOT NULL,
    location VARCHAR(255),
    remnant INT(1) DEFAULT 0,
    article_id INT(11),
    PRIMARY KEY (id),
    FOREIGN KEY (article_id) REFERENCES articles (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `parts` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    length INTEGER NOT NULL,
    width INTEGER NOT NULL,
    xpos INTEGER NOT NULL,
    ypos INTEGER NOT NULL,
    roll_id INT(11),
    PRIMARY KEY (id),
    FOREIGN KEY (roll_id) REFERENCES rolls (id) ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `orders` (
    id INT(11) NOT NULL AUTO_INCREMENT,
    date DATE NOT NULL,
    length INTEGER NOT NULL,
    width INTEGER NOT NULL,
    orderNumber VARCHAR(255),
    roll_id INT(11),
    client_id INT(11),
    PRIMARY KEY (id),
    FOREIGN KEY (roll_id) REFERENCES rolls (id) ON DELETE SET NULL,
    FOREIGN KEY (client_id) REFERENCES clients (id)
) ENGINE = InnoDB;