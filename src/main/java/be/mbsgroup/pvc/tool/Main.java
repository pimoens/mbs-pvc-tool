package be.mbsgroup.pvc.tool;

import java.util.HashMap;

import be.mbsgroup.pvc.tool.jdbc.JDBCUserDAO;
import be.mbsgroup.pvc.tool.models.User;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Pieter Moens
 */
public class Main extends Application {

    public static HashMap<String, Scene> scenes = new HashMap<>();
    public static HashMap<String, FXMLLoader> loaders = new HashMap<>();

    @Override
    public void start(Stage stage) throws Exception {
        Parent loginScene = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
        scenes.put("Login", new Scene(loginScene));
        FXMLLoader mainSceneLoader = new FXMLLoader(getClass().getResource("/fxml/Main.fxml"));
        loaders.put("Main", mainSceneLoader);
        scenes.put("Main", new Scene(mainSceneLoader.load()));

        User user = new User("PVC", "CVP");
        JDBCUserDAO.getInstance().insertOrUpdate(user);

        stage.setScene(scenes.get("Login"));
        stage.setResizable(false);
        stage.show();

        FXMLLoader manageArticleLoader = new FXMLLoader(getClass().getResource("/fxml/ArticleDialog.fxml"));
        FXMLLoader rollDialogLoader = new FXMLLoader(getClass().getResource("/fxml/RollDialog.fxml"));
        FXMLLoader orderDialogLoader = new FXMLLoader(getClass().getResource("/fxml/OrderDialog.fxml"));
        loaders.put("ManageArticle", manageArticleLoader);
        loaders.put("RollDialog", rollDialogLoader);
        loaders.put("OrderDialog", orderDialogLoader);
        scenes.put("ManageArticle", new Scene(manageArticleLoader.load()));
        scenes.put("RollDialog", new Scene(rollDialogLoader.load()));
        scenes.put("OrderDialog", new Scene(orderDialogLoader.load()));
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
