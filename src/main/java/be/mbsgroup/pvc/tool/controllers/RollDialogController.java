/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.controllers;

import static be.mbsgroup.pvc.tool.Main.loaders;
import be.mbsgroup.pvc.tool.jdbc.JDBCPartDAO;
import be.mbsgroup.pvc.tool.jdbc.JDBCRollDAO;
import be.mbsgroup.pvc.tool.models.Article;
import be.mbsgroup.pvc.tool.models.Part;
import be.mbsgroup.pvc.tool.models.Roll;
import be.mbsgroup.pvc.tool.views.WorkBench;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Pieter Moens
 */
public class RollDialogController implements Initializable {

    private Article article = null;

    @FXML
    private Label errorLabel;
    @FXML
    private DatePicker rollDate;
    @FXML
    private TextField rollLength, rollWidth, rollLocation;
    @FXML
    private CheckBox rollRemnant;
    @FXML
    private Button addRollButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rollDate.setValue(LocalDate.now());
    }

    @FXML
    public void handleAddRollButton(Event event) throws IOException {
        try {
            Roll roll = new Roll();
            Part part = new Part();

            roll.setArticle_id(article.getId());
            Date date = Date.from(rollDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
            roll.setDate(new java.sql.Date(date.getTime()));
            roll.setLocation(rollLocation.getText().trim());
            roll.setRemnant(rollRemnant.isSelected());

            int length = Integer.parseInt(rollLength.getText().trim());
            part.setLength(length);
            int width = Integer.parseInt(rollWidth.getText().trim());
            part.setWidth(width);
            
            if (length == 0 || width == 0) {
                errorLabel.setText("Length and width should be higher than 0.");
            }

            roll = JDBCRollDAO.getInstance().insertOrUpdate(roll);
            part.setRoll(roll);
            part = JDBCPartDAO.getInstance().insertOrUpdate(part);
            roll.addPart(part);
            article.addRoll(roll);
        } catch (NumberFormatException e) {
            errorLabel.setText("Length and width must be integers!");
        }

        MainController controller = loaders.get("Main").<MainController>getController();
        controller.update();
        
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
