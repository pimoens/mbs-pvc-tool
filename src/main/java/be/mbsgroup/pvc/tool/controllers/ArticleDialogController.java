/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.controllers;

import static be.mbsgroup.pvc.tool.Main.loaders;
import be.mbsgroup.pvc.tool.jdbc.JDBCArticleDAO;
import be.mbsgroup.pvc.tool.jdbc.JDBCClientDAO;
import be.mbsgroup.pvc.tool.jdbc.JDBCSupplierDAO;
import be.mbsgroup.pvc.tool.jdbc.JDBCUserDAO;
import be.mbsgroup.pvc.tool.models.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Pieter Moens
 */
public class ArticleDialogController implements Initializable {

    private final JDBCArticleDAO articledb = JDBCArticleDAO.getInstance();
    private final JDBCSupplierDAO supplierdb = JDBCSupplierDAO.getInstance();
    private final JDBCUserDAO userdb = JDBCUserDAO.getInstance();

    private HashMap<String, Supplier> supplierMap = new HashMap<>();

    private Article article;
    private Category category;

    @FXML
    private Label errorLabel;
    @FXML
    private TextField articleCode, articleDescription, articlePrice, articleOrderNumber;
    @FXML
    private AutoCompleteTextField articleSupplier;
    @FXML
    private ChoiceBox articleManagedBy;
    @FXML
    private CheckBox articleConsignment;
    @FXML
    private Button manageArticleButton;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initializeSuppliers();
        initializeManagers();

        setData();
    }

    private void setData() {
        articleCode.clear();
        articleDescription.clear();
        articlePrice.clear();
        articleSupplier.clear();
        articleOrderNumber.clear();
        articleManagedBy.getSelectionModel().selectFirst();
        articleConsignment.setSelected(false);
        manageArticleButton.setText("Create article");
        errorLabel.setText("");
    }

    @FXML
    public void handleManageArticleButton(Event event) throws IOException {
        try {
            String code = articleCode.getText().trim();
            if (code.equals("")) {
                errorLabel.setText("Article code required.");
            }
            article.setCode(code);
            article.setDescription(articleDescription.getText().trim());
            
            double price;
            if (articlePrice.getText().trim().isEmpty()) price = 0;
            else price = Double.parseDouble(articlePrice.getText());
            article.setUnitPrice(price);

            Supplier supplier = supplierMap.get(articleSupplier.getText().trim());
            if (supplier == null) {
                errorLabel.setText("Select an existing supplier.");
            }


            article.setOrderNumber(articleOrderNumber.getText().trim());
            article.setSupplier(supplier);
            article.setManagedBy((User) articleManagedBy.getValue());
            article.setConsignment(articleConsignment.isSelected());
            article.setCategory(this.category);

            article = articledb.insertOrUpdate(article);
            MainController controller = loaders.get("Main").<MainController>getController();
            controller.addArticleItem(article);

            Node source = (Node) event.getSource();
            Stage stage = (Stage) source.getScene().getWindow();
            stage.close();

            setData();
        } catch (NumberFormatException e) {
            errorLabel.setText("Price is not valid. (e.g.: 29.30)");
        }
    }

    private void initializeSuppliers() {
        ArrayList<Supplier> suppliers = supplierdb.selectAll();

        suppliers.forEach(supplier -> supplierMap.put(supplier.getName(), supplier));
        articleSupplier.getEntries().addAll(supplierMap.keySet());
    }

    private void initializeManagers() {
        ArrayList<User> users = userdb.selectAll();
        users.forEach(user -> articleManagedBy.getItems().add(user));
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
        this.category = article.getCategory();

        articleCode.setText(article.getCode());
        articleDescription.setText(article.getDescription());
        articlePrice.setText(Double.toString(article.getUnitPrice()));
        articleSupplier.setText(article.getSupplier().getName());
        articleOrderNumber.setText(article.getOrderNumber());
        articleManagedBy.getSelectionModel().select(article.getManagedBy());
        articleConsignment.setSelected(article.isConsignment());
        manageArticleButton.setText("Edit article");
        errorLabel.setText("");
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
        article = new Article();
        setData();
    }
}
