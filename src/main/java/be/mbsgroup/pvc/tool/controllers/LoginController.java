/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.controllers;

import be.mbsgroup.pvc.tool.Main;
import be.mbsgroup.pvc.tool.jdbc.JDBCUserDAO;
import be.mbsgroup.pvc.tool.models.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Pieter Moens
 */
public class LoginController implements Initializable {

    @FXML
    private ImageView logo;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button loginButton;
    @FXML
    private Label errorLabel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void handleLoginButtonAction(Event event) throws IOException {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if (loginUser(username, password)) {
            Stage login = (Stage) loginButton.getScene().getWindow();
            login.close();

            Stage main = new Stage();
            main.setTitle("MBS - PVC Inventory Tool");
            main.setMaximized(true);
            main.setScene(Main.scenes.get("Main"));
            main.show();
        } else {
            errorLabel.setText("Invalid username and/or password.");
        }
    }

    private boolean loginUser(String username, String password) {
        JDBCUserDAO userdb = JDBCUserDAO.getInstance();
        User temp = userdb.selectByUsername(username);

        if (temp != null) {
            return temp.comparePassword(password);
        } else {
            return false;
        }
    }
}
