package be.mbsgroup.pvc.tool.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static be.mbsgroup.pvc.tool.Main.loaders;
import be.mbsgroup.pvc.tool.jdbc.JDBCClientDAO;
import be.mbsgroup.pvc.tool.models.Client;
import be.mbsgroup.pvc.tool.models.Order;
import be.mbsgroup.pvc.tool.models.Roll;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Pieter Moens
 */
public class OrderDialogController implements Initializable {

    private Roll roll;
    private HashMap<String, Client> clientMap = new HashMap<>();

    @FXML
    private DatePicker orderDate;
    @FXML
    private TextField orderLength, orderWidth, orderNumber;
    @FXML
    private AutoCompleteTextField orderClient;
    @FXML
    private Label errorLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        orderDate.setValue(LocalDate.now());
        initializeClients();
    }

    @FXML
    private void handleCreateOrderButton(Event event) {
        Order order = new Order();

        try {
            order.setDate(Date.from(orderDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
            order.setOrderNumber(orderNumber.getText().trim());
            Client client = clientMap.get(orderClient.getText().trim());

            int length = Integer.parseInt(orderLength.getText().trim());
            order.setLength(length);
            int width = Integer.parseInt(orderWidth.getText().trim());
            order.setWidth(width);

            if (length <= 0 || width <= 0) {
                errorLabel.setText("Length and width have to be higher than '0'.");
            } else if (client == null) {
                errorLabel.setText("Select an existing client.");
            } else {
                order.setClient(client);
                order.setRoll(this.roll);

                MainController controller = loaders.get("Main").<MainController>getController();
                controller.setOrder(order);

                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
                
                errorLabel.setText("");
            }
        } catch (NumberFormatException e) {
            errorLabel.setText("Length and width must be integers!");
        }
    }

    private void initializeClients() {
        ArrayList<Client> clients = JDBCClientDAO.getInstance().selectAll();

        clients.forEach(client -> clientMap.put(client.getName(), client));
        orderClient.getEntries().addAll(clientMap.keySet());
    }

    public Roll getRoll() {
        return roll;
    }

    public void setRoll(Roll roll) {
        this.roll = roll;
    }
}
