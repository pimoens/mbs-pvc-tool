/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.controllers;

import be.mbsgroup.pvc.tool.Main;
import be.mbsgroup.pvc.tool.models.Article;
import be.mbsgroup.pvc.tool.models.Order;
import be.mbsgroup.pvc.tool.views.CategoryMenu;
import be.mbsgroup.pvc.tool.views.WorkBench;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Pieter Moens
 */
public class MainController implements Initializable {

    @FXML
    private MenuBar mainMenu;;
    @FXML
    private Parent embeddedCategoryMenu, embeddedWorkBench;
    @FXML
    private CategoryMenu embeddedCategoryMenuController;
    @FXML
    private WorkBench embeddedWorkBenchController;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        embeddedCategoryMenuController.initialize();
        embeddedWorkBenchController.initialize();
    }

    @FXML
    private void handleLogoutButtonAction(ActionEvent event) throws IOException {
        Stage main = (Stage) mainMenu.getScene().getWindow();
        main.close();
        
        Stage login = new Stage();
        login.setScene(Main.scenes.get("Login"));
        login.sizeToScene();
        login.centerOnScreen();
        login.setResizable(false);
        login.show();
    }
    
    public void addArticleItem(Article article) {
        embeddedCategoryMenuController.addArticleItem(article);
    }
    
    public void setArticle(Article article) {
        embeddedWorkBenchController.setArticle(article);
    }
    
    public void setOrder(Order order) {
        embeddedWorkBenchController.setOrder(order);
    }
    
    public void update() {
        embeddedWorkBenchController.initialize();
    }
}