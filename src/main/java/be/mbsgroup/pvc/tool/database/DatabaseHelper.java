/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.database;

import com.mysql.jdbc.jdbc2.optional.*;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class DatabaseHelper {

    private static final DatabaseHelper INSTANCE = new DatabaseHelper();
    private static MysqlDataSource DATASOURCE;

    public DatabaseHelper() {
        this.connect();
    }

    public static DatabaseHelper getInstance() {
        return INSTANCE;
    }

    private void connect() {
        try {
            Properties properties = new Properties();
            InputStream s = this.getClass().getResourceAsStream("/config/database.config");
            properties.load(s);

            DATASOURCE = new MysqlDataSource();

            DATASOURCE.setServerName(properties.getProperty("HOST"));
            DATASOURCE.setUser(properties.getProperty("USER"));
            DATASOURCE.setPassword(properties.getProperty("PASSWORD"));
            DATASOURCE.setDatabaseName(properties.getProperty("DATABASE"));

            // Testing connection.
            Connection c = DATASOURCE.getConnection();
            c.close();
        } catch (IOException ex) {
            System.out.println("[!!] Database configuration not found.");
        } catch (SQLException ex) {
            System.out.println("[!!] Could not connect to the database.");
        }
    }

    public int update(String query, boolean return_id) throws SQLException {
        Connection c = DATASOURCE.getConnection();
        Statement stm = c.createStatement();

        int affectedRows = stm.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
        if (affectedRows == 0) {
            throw new SQLException("No affected rows.");
        } else {
            if (return_id) {
                try (ResultSet generatedKeys = stm.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                }
            }
        }

        c.close();
        return affectedRows;
    }

    public ArrayList<Map<String, Object>> fetchAssoc(String query) {
        try {
            Connection c = DATASOURCE.getConnection();
            Statement stm = c.createStatement();

            ResultSet rs = stm.executeQuery(query);

            ResultSetMetaData meta = rs.getMetaData();
            ArrayList<String> cols = new ArrayList<>();;
            for (int i = 1; i <= meta.getColumnCount(); i++) {
                cols.add(meta.getColumnName(i));
            }

            ArrayList<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
            while (rs.next()) {
                Map<String, Object> row = new HashMap<String, Object>();
                for (String col : cols) {
                    row.put(col, rs.getObject(col));
                }
                rows.add(row);
            }

            c.close();

            return rows;
        } catch (SQLException e) {
            System.out.println("[!!] SQLException!");
            return null;
        }
    }

    public Connection getConnection() throws SQLException {
        return DATASOURCE.getConnection();
    }
}
