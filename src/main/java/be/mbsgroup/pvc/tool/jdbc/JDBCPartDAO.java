/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.PartDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.database.NamedParameterStatement;
import be.mbsgroup.pvc.tool.models.Part;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class JDBCPartDAO implements PartDAO {

    private static final JDBCPartDAO INSTANCE = new JDBCPartDAO();
    private static final String TABLENAME = "parts";
    private static DatabaseHelper DB_HELPER;

    public JDBCPartDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCPartDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Part> selectAll() {
        String sql = String.format("SELECT id, length, width, xpos, ypos FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Part> parts = new ArrayList<>();
        result.forEach((row) -> {
            parts.add(mapResultToObject(row));
        });

        return parts;
    }

    @Override
    public Part selectById(int id) {
        String sql = String.format("SELECT id, length, width, xpos, ypos FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            return mapResultToObject(result.get(0));
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<Part> selectAllByRollId(int id) {
        String sql = String.format("SELECT id, length, width, xpos, ypos FROM %s WHERE roll_id=%d",
                TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Part> parts = new ArrayList<>();
        result.forEach((row) -> {
            parts.add(mapResultToObject(row));
        });

        return parts;
    }

    @Override
    public Part insertOrUpdate(Part part) {
        try {
            String query = "INSERT INTO parts (id, length, width, xpos, ypos, roll_id) "
                    + "VALUES (:id, :length, :width, :xpos, :ypos, :roll_id) "
                    + "ON DUPLICATE KEY UPDATE length=:length, width=:width, xpos=:xpos, ypos=:ypos, roll_id=:roll_id";
            NamedParameterStatement pst = new NamedParameterStatement(DB_HELPER.getConnection(), query);
            pst.setInt("id", part.getId());
            pst.setInt("length", part.getLength());
            pst.setInt("width", part.getWidth());
            pst.setInt("xpos", part.getXpos());
            pst.setInt("ypos", part.getYpos());
            pst.setInt("roll_id", part.getRoll().getId());
            int id = pst.executeUpdate();
            part.setId(id);
            return part;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCRollDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public int deletePart(Part part) {
        try {
            String sql = String.format("DELETE FROM %s WHERE id=%d", TABLENAME, part.getId());

            return DB_HELPER.update(sql, false);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCRollDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return -1;
    }

    private Part mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();

        if (result != null) {
            Part part = mapper.convertValue(result, Part.class);
            part.updateBounds();
            return part;
        } else {
            return null;
        }
    }

}
