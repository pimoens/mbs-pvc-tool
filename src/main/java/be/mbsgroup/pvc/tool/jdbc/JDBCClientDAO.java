/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.ClientDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.models.Article;
import be.mbsgroup.pvc.tool.models.Client;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Pieter Moens
 */
public class JDBCClientDAO implements ClientDAO {

    private static final JDBCClientDAO INSTANCE = new JDBCClientDAO();
    private static final String TABLENAME = "clients";
    private static DatabaseHelper DB_HELPER;

    public JDBCClientDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCClientDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Client> selectAll() {
        String sql = String.format("SELECT * FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Client> clients = new ArrayList<>();
        result.forEach((row) -> {
            clients.add(mapResultToObject(row));
        });

        return clients;
    }

    @Override
    public Client selectById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            Client client = mapResultToObject(result.get(0));
            return client;
        } else {
            return null;
        }
    }

    @Override
    public Client selectByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertOrUpdate(Client client) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Client mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();

        if (result != null) {
            Client client = mapper.convertValue(result, Client.class);
            return client;
        } else {
            return null;
        }
    }
}
