/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.ArticleDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.models.Article;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class JDBCArticleDAO implements ArticleDAO {

    private static final JDBCArticleDAO INSTANCE = new JDBCArticleDAO();
    private static final String TABLENAME = "articles";
    private static DatabaseHelper DB_HELPER;

    public JDBCArticleDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCArticleDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Article> selectAll() {
        String sql = String.format("SELECT id, code, description, unitPrice, orderNumber, consignment FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Article> articles = new ArrayList<>();
        for (Map<String, Object> row : result) {
            articles.add(mapResultToObject(row));
        }

        return articles;
    }

    @Override
    public Article selectById(int id) {
        String sql = String.format("SELECT id, code, description, unitPrice, orderNumber, consignment FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            return mapResultToObject(result.get(0));
        } else {
            return null;
        }
    }

    @Override
    public Article insertOrUpdate(Article article) {
        try {
            String sql;
            sql = MessageFormat.format("INSERT INTO {0} (id, code, description, unitPrice, orderNumber, user_id, supplier_id, category_id, consignment) "
                    + "VALUES ({1}, ''{2}'', ''{3}'', ''{4}'', ''{5}'', {6}, {7}, {8}, {9}) "
                    + "ON DUPLICATE KEY UPDATE code=''{2}'', description=''{3}'', unitPrice=''{4}'', orderNumber=''{5}'', user_id={6}, supplier_id={7}, category_id={8}, consignment={9}",
                    TABLENAME, article.getId(), article.getCode(), article.getDescription(),
                    Double.toString(article.getUnitPrice()), article.getOrderNumber(), article.getManagedBy().getId(),
                    article.getSupplier().getId(), article.getCategory().getId(), article.isConsignment());
            int id = DB_HELPER.update(sql, true);
            article.setId(id);
            return article;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public int deleteArticle(Article article) {
        try {
            String sql = String.format("DELETE FROM %s WHERE id=%d", TABLENAME, article.getId());
            
            return DB_HELPER.update(sql, false);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCArticleDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    private Article addExternalInfoToArticle(Article article) {
        String sql = String.format("SELECT category_id, user_id, supplier_id FROM %s WHERE id=%d", TABLENAME, article.getId());
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            Map<String, Object> row = result.get(0);
            article.setCategory(JDBCCategoryDAO.getInstance().selectById((int) row.get("category_id")));
            article.setManagedBy(JDBCUserDAO.getInstance().selectById((int) row.get("user_id")));
            article.setSupplier(JDBCSupplierDAO.getInstance().selectById((int) row.get("supplier_id")));
            article.setRolls(JDBCRollDAO.getInstance().selectAllByArticleId(article.getId()));
        } else {
            return null;
        }

        return article;
    }

    private Article mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();

        if (result != null) {
            Article article = mapper.convertValue(result, Article.class);
            return addExternalInfoToArticle(article);
        } else {
            return null;
        }
    }
}
