/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.CategoryDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.models.Category;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class JDBCCategoryDAO implements CategoryDAO {

    private static final JDBCCategoryDAO INSTANCE = new JDBCCategoryDAO();
    private static final String TABLENAME = "categories";
    private static DatabaseHelper DB_HELPER;

    public JDBCCategoryDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCCategoryDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Category> selectAll() {
        String sql = String.format("SELECT * FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Category> categories = new ArrayList<>();
        result.forEach((row) -> {
            categories.add(mapResultToObject(row));
        });

        return categories;
    }

    @Override
    public Category selectById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            return mapResultToObject(result.get(0));
        } else {
            return null;
        }
    }
    
    public ArrayList<Category> selectChildren(int id) {
        String sql = String.format("SELECT * FROM %s WHERE parent_id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ObjectMapper mapper = new ObjectMapper();

        ArrayList<Category> categories = new ArrayList<>();
        result.forEach((row) -> {
            categories.add(mapResultToObject(row));
        });

        return categories;
    }

    @Override
    public Category insertOrUpdate(Category category) {
        try {
            String sql = MessageFormat.format("INSERT INTO {0} (id, name, parent_id) "
                    + "VALUES ({1}, ''{2}'', {3}) "
                    + "ON DUPLICATE KEY UPDATE name=''{2}'', parent_id={3}",
                    TABLENAME, category.getId(), category.getName(), category.getParent_id());
            
            int id = DB_HELPER.update(sql, true);
            category.setId(id);
            return category;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCCategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public int deleteCategory(Category category) {
        try {
            String sql = String.format("DELETE FROM %s WHERE id=%d", TABLENAME, category.getId());
            
            return DB_HELPER.update(sql, false);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCCategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return -1;
    }

    private Category mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();

        if (result != null) {
            Category category = mapper.convertValue(result, Category.class);
            return category;
        } else {
            return null;
        }
    }
}
