/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.OrderDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.database.NamedParameterStatement;
import be.mbsgroup.pvc.tool.models.Order;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class JDBCOrderDAO implements OrderDAO {

    private static final JDBCOrderDAO INSTANCE = new JDBCOrderDAO();
    private static final String TABLENAME = "orders";
    private static DatabaseHelper DB_HELPER;

    public JDBCOrderDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCOrderDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Order> selectAll() {
        String sql = String.format("SELECT id, name, code, unitPrice, orderNumber, consignment FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Order> orders = new ArrayList<>();
        for (Map<String, Object> row : result) {
            orders.add(mapResultToObject(row));
        }

        return orders;
    }

    @Override
    public Order selectById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            return mapResultToObject(result.get(0));
        } else {
            return null;
        }
    }

    @Override
    public Order insertOrUpdate(Order order) {
        try {
            String query = "INSERT INTO orders (id, date, length, width, orderNumber, roll_id, client_id) "
                    + "VALUES (:id, :date, :length, :width, :orderNumber, :roll_id, :client_id) "
                    + "ON DUPLICATE KEY UPDATE date=:date, length=:length, "
                    + "width=:width, orderNumber=:orderNumber, roll_id=:roll_id, client_id=:client_id";
            NamedParameterStatement pst = new NamedParameterStatement(DB_HELPER.getConnection(), query);
            pst.setInt("id", order.getId());
            pst.setObject("date", new java.sql.Date(order.getDate().getTime()));
            pst.setInt("length", order.getLength());
            pst.setInt("width", order.getWidth());
            pst.setString("orderNumber", order.getOrderNumber());
            pst.setInt("roll_id", order.getRoll().getId());
            pst.setInt("client_id", order.getClient().getId());
            int id = pst.executeUpdate();
            order.setId(id);
            
            return order;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    private Order addExternalInfoToOrder(Order order) {
        String sql = String.format("SELECT roll_id, client_id FROM %s WHERE id=%d", TABLENAME, order.getId());
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            Map<String, Object> row = result.get(0);
            order.setRoll(JDBCRollDAO.getInstance().selectById((int) row.get("roll_id")));
            order.setClient(JDBCClientDAO.getInstance().selectById((int) row.get("client_id")));
        } else {
            return null;
        }

        return order;
    }

    private Order mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();

        if (result != null) {
            Order order = mapper.convertValue(result, Order.class);
            return addExternalInfoToOrder(order);
        } else {
            return null;
        }
    }
}
