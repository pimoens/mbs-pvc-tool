/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.RoleDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.models.Role;
import be.mbsgroup.pvc.tool.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class JDBCRoleDAO implements RoleDAO {

    private static final JDBCRoleDAO INSTANCE = new JDBCRoleDAO();
    private static final String TABLENAME = "roles";
    private static DatabaseHelper DB_HELPER;

    public JDBCRoleDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCRoleDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Role> selectAll() {
        String sql = String.format("SELECT * FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Role> roles = new ArrayList<>();
        result.forEach((row) -> {
            roles.add(mapResultToObject(row));
        });
        
        return roles;
    }
    
    @Override
    public ArrayList<User> selectAllUsersWithRole(Role role) {
        String sql = String.format("SELECT * FROM users_roles WHERE role_id", TABLENAME, role.getId());
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);
        
        ObjectMapper mapper = new ObjectMapper();
                
        if (result != null && result.size() > 0) {
            ArrayList<User> users = new ArrayList<>();
            result.forEach((row) -> {
                users.add(mapper.convertValue(row, User.class));
            });
            return users;
        } else {
            return null;
        }
    }

    @Override
    public Role selectById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);
        
        if (result != null && result.size() > 0) {
            return mapResultToObject(result.get(0));
        } else return null;
    }

    @Override
    public Role selectByName(String name) {
        String sql = String.format("SELECT * FROM %s WHERE name='%s'", TABLENAME, name);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);
        
        if (result != null && result.size() > 0) {
            return mapResultToObject(result.get(0));
        } else return null;
    }


    @Override
    public Role insertOrUpdate(Role role) {
        try {
            String sql = MessageFormat.format("INSERT INTO {0} (id, name) "
                    + "VALUES ({1}, ''{2}'') "
                    + "ON DUPLICATE KEY UPDATE name=''{2}''",
                    TABLENAME, role.getId(), role.getName());
            
            int id = DB_HELPER.update(sql, true);
            role.setId(id);
            return role;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCRoleDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private Role mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();
                
        if (result != null) {
            Role role =  mapper.convertValue(result, Role.class);
            return role;
        } else {
            return null;
        }
    }
}

