/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.SupplierDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.models.Supplier;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Pieter Moens
 */
public class JDBCSupplierDAO implements SupplierDAO {

    private static final JDBCSupplierDAO INSTANCE = new JDBCSupplierDAO();
    private static final String TABLENAME = "suppliers";
    private static DatabaseHelper DB_HELPER;

    public JDBCSupplierDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCSupplierDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Supplier> selectAll() {
        String sql = String.format("SELECT * FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Supplier> suppliers = new ArrayList<>();
        result.forEach((row) -> {
            suppliers.add(mapResultToObject(row));
        });

        return suppliers;
    }

    @Override
    public Supplier selectById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            Supplier supplier = mapResultToObject(result.get(0));
            return supplier;
        } else {
            return null;
        }
    }

    @Override
    public Supplier selectByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int insertOrUpdate(Supplier supplier) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Supplier mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();

        if (result != null) {
            Supplier supplier = mapper.convertValue(result, Supplier.class);
            return supplier;
        } else {
            return null;
        }
    }
}
