/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.UserDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.models.Role;
import be.mbsgroup.pvc.tool.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class JDBCUserDAO implements UserDAO {

    private static final JDBCUserDAO INSTANCE = new JDBCUserDAO();
    private static final String TABLENAME = "users";
    private static DatabaseHelper DB_HELPER;

    public JDBCUserDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCUserDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<User> selectAll() {
        String sql = String.format("SELECT id, username, email, hashedPassword FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<User> users = new ArrayList<>();
        result.forEach((row) -> {
            users.add(mapResultToObject(row));
        });
        
        return users;
    }

    @Override
    public User selectById(int id) {
        String sql = String.format("SELECT id, username, email, hashedPassword FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);
        
        if (result != null && result.size() > 0) {
            User user = mapResultToObject(result.get(0));
            return user;
        } else return null;
    }

    @Override
    public User selectByUsername(String username) {
        String sql = String.format("SELECT id, username, email, hashedPassword FROM %s WHERE username='%s'", TABLENAME, username);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);
        
        if (result != null && result.size() > 0) {
            User user = mapResultToObject(result.get(0));
            return user;
        } else return null;
    }

    @Override
    public User insertOrUpdate(User user) {
        try {
            String sql = MessageFormat.format("INSERT INTO {0} (id, username, email, hashedPassword) "
                    + "VALUES ({1}, ''{2}'', ''{3}'', ''{4}'') "
                    + "ON DUPLICATE KEY UPDATE username=''{2}'', email=''{3}'', hashedPassword=''{4}''",
                    TABLENAME, user.getId(), user.getUsername(), user.getEmail(), user.getHashedPassword());
            
            for (Role role : user.getRoles()) {
                String sql_fetch = String.format("SELECT * FROM users_roles WHERE user_id=%d AND role_id=%d",
                        user.getId(), role.getId());
                
                ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql_fetch);
                if (result.isEmpty()) {
                    String sql_insert = String.format("INSERT INTO users_roles (user_id, role_id) VALUES (%d, %d)",
                            user.getId(), role.getId());
                    
                    DB_HELPER.update(sql_insert, true);
                }
            }
            
            user.setId(DB_HELPER.update(sql, true));
            return user;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCUserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private User mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();
                
        if (result != null) {
            User user =  mapper.convertValue(result, User.class);   
            return addRolesToUser(user);
        } else {
            return null;
        }
    }    
    
    private ArrayList<Role> selectAllRolesFromUser(User user) {
        String sql = String.format("SELECT role_id FROM users_roles WHERE user_id=%d", user.getId());  
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);
        
        ArrayList<Role> roles = new ArrayList<>();
        for (Map<String, Object> row : result) {
            roles.add(JDBCRoleDAO.getInstance().selectById((int) row.get("role_id")));
        }
        
        return roles;
    }
    
    
    private User addRolesToUser(User user) {
        ArrayList<Role> roles = selectAllRolesFromUser(user);
        for (Role role : roles) {
            user.addRole(role);
        }
        
        return user;
    }
}
