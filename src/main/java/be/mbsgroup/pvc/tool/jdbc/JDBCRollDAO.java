/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.jdbc;

import be.mbsgroup.pvc.tool.dao.RollDAO;
import be.mbsgroup.pvc.tool.database.DatabaseHelper;
import be.mbsgroup.pvc.tool.database.NamedParameterStatement;
import be.mbsgroup.pvc.tool.models.Part;
import be.mbsgroup.pvc.tool.models.Roll;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pieter Moens
 */
public class JDBCRollDAO implements RollDAO {

    private static final JDBCRollDAO INSTANCE = new JDBCRollDAO();
    private static final String TABLENAME = "rolls";
    private static DatabaseHelper DB_HELPER;

    public JDBCRollDAO() {
        DB_HELPER = DatabaseHelper.getInstance();
    }

    public static JDBCRollDAO getInstance() {
        return INSTANCE;
    }

    @Override
    public ArrayList<Roll> selectAll() {
        String sql = String.format("SELECT * FROM %s", TABLENAME);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Roll> rolls = new ArrayList<>();
        result.forEach((row) -> {
            rolls.add(mapResultToObject(row));
        });

        return rolls;
    }

    @Override
    public Roll selectById(int id) {
        String sql = String.format("SELECT * FROM %s WHERE id=%d", TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        if (result != null && result.size() > 0) {
            return mapResultToObject(result.get(0));
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<Roll> selectAllByArticleId(int id) {
        String sql = String.format("SELECT * FROM %s WHERE article_id=%d",
                TABLENAME, id);
        ArrayList<Map<String, Object>> result = DB_HELPER.fetchAssoc(sql);

        ArrayList<Roll> rolls = new ArrayList<>();
        result.forEach((row) -> {
            rolls.add(mapResultToObject(row));
        });

        return rolls;
    }

    @Override
    public Roll insertOrUpdate(Roll roll) {
        try {
            String query = "INSERT INTO rolls (id, date, location, remnant, article_id) "
                    + "VALUES (:id, :date, :location, :remnant, :article_id) "
                    + "ON DUPLICATE KEY UPDATE date=:date, location=:location, remnant=:remnant, article_id=:article_id";
            NamedParameterStatement pst = new NamedParameterStatement(DB_HELPER.getConnection(), query);
            pst.setInt("id", roll.getId());
            pst.setObject("date", new java.sql.Date(roll.getDate().getTime()));
            pst.setString("location", roll.getLocation());
            pst.setObject("remnant", roll.isRemnant());
            pst.setInt("article_id", roll.getArticle_id());
            int id = pst.executeUpdate();
            if (id != 0) roll.setId(id);

            ArrayList<Part> existingParts = JDBCPartDAO.getInstance().selectAllByRollId(roll.getId());
            existingParts.forEach(part -> {
                if (!roll.getParts().contains(part)) JDBCPartDAO.getInstance().deletePart(part);
            });
            roll.getParts().forEach(part -> {
                part.setRoll(roll);
                JDBCPartDAO.getInstance().insertOrUpdate(part);
            });

            return roll;
        } catch (SQLException ex) {
            Logger.getLogger(JDBCRollDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public int deleteRoll(Roll roll) {
        try {
            String sql = String.format("DELETE FROM %s WHERE id=%d", TABLENAME, roll.getId());
            
            return DB_HELPER.update(sql, false);
        } catch (SQLException ex) {
            Logger.getLogger(JDBCRollDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return -1;
    }
    
    private Roll addExternalInfoToRoll(Roll roll) {
        ArrayList<Part> parts = JDBCPartDAO.getInstance().selectAllByRollId(roll.getId());
        roll.setParts(parts);

        return roll;
    }

    private Roll mapResultToObject(Map<String, Object> result) {
        ObjectMapper mapper = new ObjectMapper();

        if (result != null) {
            Roll roll = mapper.convertValue(result, Roll.class);
            return addExternalInfoToRoll(roll);
        } else {
            return null;
        }
    }
}
