/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.models;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;

/**
 *
 * @author Pieter Moens
 */
public class Part {
    private int id;
    private int length;
    private int width;
    private int xpos;
    private int ypos;
    private PartStatus status;
    private boolean selected;
    private Roll roll;
    private Bounds bounds;

    public Part() {
        updateBounds();
    }

    public Part(int length, int width) {
        this.length = length;
        this.width = width;
        updateBounds();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
        updateBounds();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        updateBounds();
    }

    public int getXpos() {
        return xpos;
    }

    public void setXpos(int xpos) {
        this.xpos = xpos;
        updateBounds();
    }

    public int getYpos() {
        return ypos;
    }

    public void setYpos(int ypos) {
        this.ypos = ypos;
        updateBounds();
    }

    public PartStatus getStatus() {
        return status;
    }

    public void setStatus(PartStatus status) {
        this.status = status;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Roll getRoll() {
        return roll;
    }

    public void setRoll(Roll roll) {
        this.roll = roll;
    }

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.xpos = (int) bounds.getMinX();
        this.ypos = (int) bounds.getMinY();
        this.width = (int) bounds.getWidth();
        this.length = (int) bounds.getHeight();

        this.bounds = bounds;
    }

    public void updateBounds() {
        this.bounds = new BoundingBox(this.xpos, this.ypos, this.width, this.length);
    }

    public void setPosition(double x, double y) {
        this.xpos = (int) x;
        this.ypos = (int) y;
        updateBounds();
    }

    @Override
    public String toString() {
        return String.format("%dx%d @ %d:%d", this.length, this.width, this.xpos, this.ypos);
    }
}
