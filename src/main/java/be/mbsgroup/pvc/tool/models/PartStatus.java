package be.mbsgroup.pvc.tool.models;

import javafx.scene.paint.Color;

public enum PartStatus {
    ROLL_DEFAULT("roll", Color.DODGERBLUE),
    ROLL_PART("roll", Color.rgb(255, 30, 30, 0.75)),
    ORDER("order", Color.FORESTGREEN),
    REMNANT("remnant", Color.rgb(255, 128, 30, 0.75)),
    GARBAGE("garbage", Color.rgb(160, 110, 255, 0.75)),
    SELECTED("", Color.rgb(60, 180 , 215));

    private String status;
    private Color color;

    private PartStatus(String status, Color color) {
        this.status = status;
        this.color = color;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return status;
    }
}
