/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.models;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public class Roll {
    private int id;
    private String rollNumber;
    private Date date;
    private String location;
    private boolean remnant;
    private int article_id;
    private ArrayList<Part> parts = new ArrayList<>();

    public Roll() {
        setDate(new java.sql.Date(new java.util.Date().getTime()));
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
        this.rollNumber = String.format("%06d", id);
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isRemnant() {
        return remnant;
    }

    public void setRemnant(boolean remnant) {
        this.remnant = remnant;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public ArrayList<Part> getParts() {
        return parts;
    }

    public void setParts(ArrayList<Part> parts) {
        this.parts = parts;
    }
    
    public void addPart(Part part) {
        this.parts.add(part);
    }
    
    public void removePart(Part part) {
        this.parts.remove(part);
    }

    public int getLength() {
        int length = 0;
        for (Part part : this.parts) length += part.getLength();
        return length;
    }

    public int getWidth() {
        int width = 0;
        for (Part part : this.parts) width += part.getWidth();
        return width;
    }
}