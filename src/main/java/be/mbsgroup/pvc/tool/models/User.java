/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.models;

import java.util.ArrayList;
import org.apache.commons.codec.digest.DigestUtils;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.AddressException;

/**
 *
 * @author Pieter Moens
 */
public final class User {

    private int id;
    private String username;
    private String email;
    private String hashedPassword;
    private final ArrayList<Role> roles = new ArrayList<>();

    public User() {
    }

    public User(String username, String password) {
        setUsername(username);
        setPassword(password);
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public String getHashedPassword() {
        return this.hashedPassword;
    }

    public String getEmail() {
        return this.email;
    }

    public ArrayList<Role> getRoles() {
        return this.roles;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.hashedPassword = hashPassword(password);
    }

    public void setEmail(String email) {
        if (isValidEmailAddress(email)) {
            this.email = email;
        }
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public boolean hasRole(Role role) {
        return this.roles.contains(role);
    }
    
    /**
     * Compare user password hash to plain text password.
     * 
     * @param password
     * @return isPasswordMatch
     */
    public boolean comparePassword(String password) {
        return this.hashedPassword.equals(hashPassword(password));
    }

    /**
     * Hash password using SHA-256.
     *
     * @param password
     * @return hashedPassword
     */
    private static String hashPassword(String password) {
        return DigestUtils.sha256Hex(password);
    }

    /**
     *
     * @param email
     * @return isValid
     */
    private static boolean isValidEmailAddress(String email) {
        boolean isValid = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            isValid = false;
        }
        return isValid;
    }
    
    @Override
    public String toString() {
        return this.username;
    }
}
