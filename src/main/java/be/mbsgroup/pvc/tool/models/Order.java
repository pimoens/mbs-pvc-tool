/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.models;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;

import java.util.Date;

/**
 *
 * @author Pieter Moens
 */
public class Order {

    private int id;
    private Date date;
    private int length;
    private int width;
    private String orderNumber;
    private Roll roll;
    private Client client;
    private Bounds bounds;

    public Order() {
        this.bounds = new BoundingBox(0.0, 0.0, 0.0, 0.0);
    }

    public Order(int length, int width) {
        this.bounds = new BoundingBox(0.0, 0.0, width, length);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
        this.bounds = new BoundingBox(this.bounds.getMinX(), this.bounds.getMinY(), this.bounds.getWidth(), length);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        this.bounds = new BoundingBox(this.bounds.getMinX(), this.bounds.getMinY(), width, this.bounds.getHeight());
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Roll getRoll() {
        return roll;
    }

    public void setRoll(Roll roll) {
        this.roll = roll;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public void setPosition(double x, double y) {
        this.bounds = new BoundingBox(x, y, this.bounds.getWidth(), this.bounds.getHeight());
    }
}
