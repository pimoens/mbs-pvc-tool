/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.views;

import static be.mbsgroup.pvc.tool.Main.loaders;
import static be.mbsgroup.pvc.tool.Main.scenes;

import be.mbsgroup.pvc.tool.controllers.OrderDialogController;
import be.mbsgroup.pvc.tool.controllers.RollDialogController;
import be.mbsgroup.pvc.tool.jdbc.JDBCOrderDAO;
import be.mbsgroup.pvc.tool.jdbc.JDBCRollDAO;
import be.mbsgroup.pvc.tool.models.*;

import java.io.IOException;
import java.sql.Date;
import java.util.*;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.*;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.commons.lang3.Range;

/**
 * @author Pieter Moens
 */
public class WorkBench {

    private Article article = null;
    private Roll roll = null;
    private Order order = null;

    private TreeSet<Part> partList = new TreeSet<>(Comparator.comparingInt(Part::getXpos));
    private ArrayList<Part> remnantList = new ArrayList<>();

    // Draw everything to the pane
    private Map<Part, StackPane> drawParts = new HashMap<>();
    private Map<Order, StackPane> drawOrders = new HashMap<>();

    // Order position
    private ArrayList<Point2D> orderPositions = new ArrayList<>();
    private int orderPosition = 0;

    // Scale
    private Scale scale = new Scale();

    // Variables for drag & drop
    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TableView<RollTableItem> rollTable;
    @FXML
    private TableColumn statusColumn, rollNumberColumn, dateColumn, locationColumn;
    @FXML
    private Label articleValue, totalStockValue, managedByValue, supplierValue, orderNumberValue, unitPriceValue, totalPriceValue;
    @FXML
    private Button addRollButton, autoButton, saveButton, newOrderButton, saveOrderButton, previousPositionButton, nextPositionButton;
    @FXML
    private ToggleButton editButton;
    @FXML
    private Pane drawPane;

    public void initialize() {
        statusColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RollTableItem, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RollTableItem, String> roll) {
                return new SimpleStringProperty(roll.getValue().getStatus());
            }
        });

        rollNumberColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RollTableItem, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RollTableItem, String> roll) {
                return new SimpleStringProperty(roll.getValue().getNumber());
            }
        });

        dateColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RollTableItem, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RollTableItem, String> roll) {
                return new SimpleStringProperty(roll.getValue().getDate());
            }
        });

        locationColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<RollTableItem, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<RollTableItem, String> roll) {
                return new SimpleStringProperty(roll.getValue().getLocation());
            }
        });

        ContextMenu rollCM = new ContextMenu();
        MenuItem editRoll = new MenuItem("Edit roll");
        editRoll.setOnAction(event -> {

        });
        rollCM.getItems().add(editRoll);
        rollTable.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
            if (t.getButton() == MouseButton.SECONDARY) {
                rollCM.show(rollTable, t.getScreenX(), t.getScreenY());
            }
        });

        rollTable.getSelectionModel().selectedItemProperty().addListener((obs, oldValue, newValue) -> {
            RollTableItem selected = newValue;

            if (selected != null) {
                roll = selected.getRoll();
                order = null;
                updateRoll();
            }
        });

//        orderStackPane.setOnMousePressed(new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(MouseEvent event) {
//                Bounds nodeInScene = orderStackPane.localToScene(orderStackPane.getBoundsInLocal());
//
//                xOffset = nodeInScene.getMinX() - event.getSceneX();
//                yOffset = nodeInScene.getMinY() - event.getSceneY();
//
//                event.consume();
//            }
//        });
//
//        orderStackPane.setOnMouseDragged((MouseEvent event) -> {
//            Bounds drawPaneInScreen = drawPane.localToScene(drawPane.getBoundsInLocal());
//
//            double newX = event.getSceneX() - drawPaneInScreen.getMinX() + xOffset;
//            double newY = event.getSceneY() - drawPaneInScreen.getMinY() + yOffset;
//
//            if (newX >= 0 && (newX + orderStackPane.getWidth()) <= drawPane.getWidth() && newY >= 0 && (newY + orderStackPane.getHeight()) <= drawPane.getHeight()) {
//                orderStackPane.setLayoutX(newX);
//                orderStackPane.setLayoutY(newY);
//            }
//
//            event.consume();
//        });

        update();
    }

    @FXML
    private void handleAddRollButton(ActionEvent event) {
        Stage rollDialog = new Stage();
        rollDialog.setScene(scenes.get("RollDialog"));
        rollDialog.show();

        RollDialogController controller = loaders.get("RollDialog").<RollDialogController>getController();
        controller.setArticle(this.article);
    }

    @FXML
    private void handleNewOrderButton(ActionEvent event) {
        Stage orderDialog = new Stage();
        orderDialog.setScene(scenes.get("OrderDialog"));
        orderDialog.show();

        OrderDialogController controller = loaders.get("OrderDialog").<OrderDialogController>getController();
        controller.setRoll(this.roll);
    }

    @FXML
    private void handleEditButton(ActionEvent event) {
        if (editButton.isSelected()) {
            remnantList.clear();
            remnantList.addAll(splitRollInParts());

            if (order != null) {
                ArrayList<Part> cutParts = new ArrayList<>();
                ArrayList<Part> remnantsToRemove = new ArrayList<>();
                for (Part remnant : remnantList) {
                    ArrayList<Part> remnantCutParts = cutOrderFromPart(remnant);

                    if (!remnantCutParts.isEmpty()) {
                        cutParts.addAll(remnantCutParts);
                        remnantsToRemove.add(remnant);
                    }
                }

                remnantList.removeAll(remnantsToRemove);
                remnantList.addAll(cutParts);
            }
        }

        drawScene();
    }

    @FXML
    private void handleAutoButton(ActionEvent event) {
        autoMergeParts();
    }

    @FXML
    private void handleSaveButton(ActionEvent event) {
        saveCurrentRoll();
    }

    @FXML
    private void handleSaveOrderButton(ActionEvent event) {
        saveCurrentRoll();
    }

    private void saveCurrentRoll() {
        ArrayList<Part> rollParts = new ArrayList<>();
        ArrayList<Part> remnants = new ArrayList<>();
        ArrayList<Part> garbage = new ArrayList<>();

        for (Part part : remnantList) {
            switch (part.getStatus()) {
                case ROLL_PART:
                    rollParts.add(part);
                    break;
                case REMNANT:
                    remnants.add(part);
                    break;
                case GARBAGE:
                    garbage.add(part);
                    break;
            }
        }

        rollParts = mergeParts(rollParts);
        remnants = mergeParts(remnants);
        garbage = mergeParts(garbage);
        if (rollValid(rollParts, null)) {
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("-- Roll %s--\n", roll.getRollNumber()));
            rollParts.forEach(rollPart -> {
                sb.append(String.format("(%dx%d)\n", rollPart.getLength(), rollPart.getWidth()));
            });

            ArrayList<Roll> remnantRolls = new ArrayList<>();
            sb.append("\n-- Remnants --\n");
            partsToRolls(remnants).forEach(remnantRoll -> {
                Roll currentRemnantRoll = new Roll();
                currentRemnantRoll.setRemnant(true);
                currentRemnantRoll.setParts(remnantRoll);
                currentRemnantRoll.setArticle_id(roll.getArticle_id());

                remnantRolls.add(currentRemnantRoll);
                sb.append(String.format("(%dx%d)\n", currentRemnantRoll.getLength(), currentRemnantRoll.getWidth()));
            });

            sb.append("\n-- Garbage --\n");
            garbage.forEach(garbagePart -> {
                sb.append(String.format("(%dx%d)\n", garbagePart.getLength(), garbagePart.getWidth()));
            });

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Warning!");
            if (order != null) alert.setHeaderText(String.format("Validate result for order: %s - (%dx%d)", order.getOrderNumber(), order.getLength(), order.getWidth()));
            else alert.setHeaderText("Validate result");
            alert.setContentText(sb.toString());

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                roll.setParts(rollParts);
                roll = JDBCRollDAO.getInstance().insertOrUpdate(roll);

                if (order != null) {
                    order.setRoll(roll);
                    JDBCOrderDAO.getInstance().insertOrUpdate(order);
                }

                remnantRolls.forEach(remnant -> {
                    remnant = JDBCRollDAO.getInstance().insertOrUpdate(remnant);
                    article.addRoll(remnant);
                });
            }

            update();
        } else {
            System.out.println("*** ROLL INVALID! ***");
            Alert invalidRollAlert = new Alert(Alert.AlertType.ERROR);
            invalidRollAlert.setTitle("Error!");
            invalidRollAlert.setHeaderText("The roll is invalid!");
        }
    }

    @FXML
    private void handlePreviousPositionButton(ActionEvent event) {
        orderPosition = (orderPosition - 1) % orderPositions.size();
        if (orderPosition < 0) orderPosition += orderPositions.size();
        order.setPosition(orderPositions.get(orderPosition).getX(), orderPositions.get(orderPosition).getY());

        drawScene();
    }

    @FXML
    private void handleNextPositionButton(ActionEvent event) {
        orderPosition = (orderPosition + 1) % orderPositions.size();
        order.setPosition(orderPositions.get(orderPosition).getX(), orderPositions.get(orderPosition).getY());

        drawScene();
    }

    private void update() {
        if (article != null) {
            editButton.setSelected(false);

            articleValue.setText(article.getCode());
            managedByValue.setText(article.getManagedBy().getUsername());
            supplierValue.setText(article.getSupplier().getName());
            orderNumberValue.setText(article.getOrderNumber());
            unitPriceValue.setText(Double.toString(article.getUnitPrice()));

            ObservableList<RollTableItem> tableItems = FXCollections.observableArrayList();

            if (article.getRolls().isEmpty()) roll = null;
            else {
                article.getRolls().forEach(roll -> {
                    if (!roll.getParts().isEmpty()) tableItems.add(new RollTableItem(roll));
                });
            }

            rollTable.setItems(tableItems);
            rollTable.getSelectionModel().select(0);
        }

        updateRoll();
    }

    /**
     * Update roll (and influenced items)
     */
    private void updateRoll() {
        partList.clear();

        if (roll != null) {
            for (Part part : roll.getParts()) {
                part.setRoll(this.roll);
                partList.add(part);
            }
        }

        updateOrder();
    }

    /**
     * Update order
     */
    private void updateOrder() {
        if (order != null) {
            calculateValidOrderLocations();
        }

        drawScene();
    }

    /**
     * Helper function to calculate all valid order locations
     */
    private void calculateValidOrderLocations() {
        orderPositions.clear();

        for (Part part : partList) {
            Bounds partBounds = part.getBounds();
            if (partBounds.getMinX() == 0 && partBounds.getWidth() >= order.getWidth()) {
                orderPositions.add(new Point2D(0, 0));
            }
            if (partBounds.getMaxX() - order.getWidth() >= 0 && partBounds.getHeight() >= order.getLength()) {
                orderPositions.add(new Point2D(partBounds.getMaxX() - order.getWidth(), partBounds.getMinY()));
            }
        }

        orderPosition = orderPositions.size() - 1;
        order.setPosition(orderPositions.get(orderPosition).getX(), orderPositions.get(orderPosition).getY());
    }

    private void drawScene() {
        if (article == null) {
            anchorPane.visibleProperty().set(false);
        } else {
            anchorPane.visibleProperty().set(true);

            if (roll == null) {
                drawPane.visibleProperty().set(false);
            } else {
                drawPane.visibleProperty().set(true);
                drawPane.setPadding(new Insets(5, 5, 5, 5));

                drawParts.clear();
                drawOrders.clear();

                calculateScale();

                if (editButton.isSelected()) {
                    for (Part remnant : remnantList) {
                        StackPane pane = createRemnantStackPane(remnant);
                        Point2D position = calculateScaledPosition(remnant.getXpos(), remnant.getYpos());
                        pane.relocate(position.getX(), position.getY());

                        drawParts.put(remnant, pane);
                    }
                } else {
                    for (Part part : partList) {
                        StackPane pane = createPartStackPane(part);
                        Point2D position = calculateScaledPosition(part.getBounds().getMinX(), part.getBounds().getMinY());
                        pane.relocate(position.getX(), position.getY());

                        drawParts.put(part, pane);
                    }
                }

                if (order != null) {
                    StackPane pane = createOrderStackPane(order);
                    Point2D position = calculateScaledPosition(order.getBounds().getMinX(), order.getBounds().getMinY());
                    pane.relocate(position.getX(), position.getY());

                    drawOrders.put(order, pane);
                }

                drawPane.getChildren().clear();
                drawParts.forEach((part, pane) -> drawPane.getChildren().add(pane));
                drawOrders.forEach((order, pane) -> drawPane.getChildren().add(pane));
            }
        }

        // Remove labels when they don't fit the StackPane
        drawPane.getChildren().forEach(child -> {
            child.applyCss();

            StackPane childPane = (StackPane) child;
            Rectangle childRect = (Rectangle) childPane.getChildren().get(0);
            Label childLabel = (Label) childPane.getChildren().get(1);

            if (childLabel.prefWidth(-1) + 10 > childRect.getWidth() || childLabel.prefHeight(-1) + 10 > childRect.getHeight()) {
                childPane.getChildren().remove(childLabel);
            }
        });

        setButtons();
    }

    /**
     * Helper function to disable/enable the buttons
     */
    private void setButtons() {
        addRollButton.setDisable(true);
        autoButton.setDisable(true);
        saveButton.setDisable(true);
        newOrderButton.setDisable(true);
        saveOrderButton.setDisable(true);
        previousPositionButton.setDisable(true);
        nextPositionButton.setDisable(true);

        if (editButton.isSelected()) {
            autoButton.setDisable(false);
            if (order == null) saveButton.setDisable(false);
            else saveOrderButton.setDisable(false);
        } else {
            if (order == null) {
                addRollButton.setDisable(false);
                newOrderButton.setDisable(false);
                editButton.setDisable(false);
            } else {
                previousPositionButton.setDisable(false);
                nextPositionButton.setDisable(false);
            }
        }
    }

    /**
     * Helper function to calculate scale based on drawPane size.
     */
    private void calculateScale() {
        int longest = 0;
        int widest = 0;

        for (Part part : roll.getParts()) {
            if (part.getLength() > longest) longest = part.getLength();
            if (part.getWidth() > widest) widest = part.getWidth();
        }

        double max_width = Math.max(300, Math.min((double) widest / 4, drawPane.getWidth() - (drawPane.getPadding().getLeft() + drawPane.getPadding().getRight())));
        double max_height = Math.max(300, Math.min((double) longest / 4, drawPane.getHeight() - (drawPane.getPadding().getTop() + drawPane.getPadding().getBottom())));

        this.scale.setX(max_width / widest);
        this.scale.setY(max_height / longest);
    }

    /**
     * Helper function to calculate the absolute position in the drawPane
     *
     * @param x
     * @param y
     * @return
     */
    private Point2D calculateScaledPosition(double x, double y) {
        return new Point2D(x * this.scale.getX() + drawPane.getPadding().getLeft(), y * this.scale.getY() + drawPane.getPadding().getTop());
    }

    private StackPane createPartStackPane(Part part) {
        Dimension2D scaledDimensions = new Dimension2D(part.getWidth() * this.scale.getX(), part.getLength() * this.scale.getY());

        Rectangle partRectangle = new Rectangle(scaledDimensions.getWidth(), scaledDimensions.getHeight(), PartStatus.ROLL_DEFAULT.getColor());
        Label partDimensions = new Label(String.format("(%dx%d)", part.getLength(), part.getWidth()));
        partDimensions.setFont(Font.font("System", 8));

        StackPane partStackPane = new StackPane(partRectangle, partDimensions);
        StackPane.setAlignment(partDimensions, Pos.TOP_LEFT);
        StackPane.setMargin(partDimensions, new Insets(5, 5, 5, 5));

        Tooltip partToolTip = new Tooltip(String.format("%06d\n(%dx%d)", roll.getId(), part.getLength(), part.getWidth()));
        Tooltip.install(partStackPane, partToolTip);

        return partStackPane;
    }

    /**
     * Helper function to create a remnant StackPane
     *
     * @param remnant
     * @return
     */
    private StackPane createRemnantStackPane(Part remnant) {
        Dimension2D scaledDimensions = new Dimension2D(remnant.getWidth() * this.scale.getX(), remnant.getLength() * this.scale.getY());

        Rectangle remnantRectangle = new Rectangle(scaledDimensions.getWidth(), scaledDimensions.getHeight());
        if (remnant.isSelected()) remnantRectangle.setFill(PartStatus.SELECTED.getColor());
        else remnantRectangle.setFill(remnant.getStatus().getColor());
        remnantRectangle.setStroke(Color.BLACK);
        remnantRectangle.setStrokeWidth(1);
        remnantRectangle.setStrokeType(StrokeType.INSIDE);
        Label remnantDim = new Label(String.format("(%dx%d)", remnant.getLength(), remnant.getWidth()));
        remnantDim.setFont(Font.font("System", 8));

        StackPane remnantStackPane = new StackPane(remnantRectangle, remnantDim);
        StackPane.setAlignment(remnantDim, Pos.TOP_LEFT);
        StackPane.setMargin(remnantDim, new Insets(5, 5, 5, 5));

        Tooltip remnantToolTip = new Tooltip(String.format("%s\n(%dx%d)", remnant.getStatus(), remnant.getLength(), remnant.getWidth()));
        Tooltip.install(remnantStackPane, remnantToolTip);

        remnantStackPane.setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                remnant.setSelected(!remnant.isSelected());
                drawScene();
            }
        });

        ContextMenu cm = new ContextMenu();
        Menu setRemnantMenu = new Menu("Set as..");
        MenuItem setAsRoll = new MenuItem("roll");
        setAsRoll.setOnAction((ActionEvent event) -> {
            remnant.setSelected(true);
            remnantList.forEach(item -> {
                if (item.isSelected()) {
                    item.setStatus(PartStatus.ROLL_PART);
                    item.setSelected(false);
                }
            });
            drawScene();
        });
        MenuItem setAsRemnant = new MenuItem("remnant");
        setAsRemnant.setOnAction((ActionEvent event) -> {
            remnant.setSelected(true);
            remnantList.forEach(item -> {
                if (item.isSelected()) {
                    item.setStatus(PartStatus.REMNANT);
                    item.setSelected(false);
                }
            });
            drawScene();
        });
        MenuItem setAsGarbage = new MenuItem("garbage");
        setAsGarbage.setOnAction((ActionEvent event) -> {
            remnant.setSelected(true);
            remnantList.forEach(item -> {
                if (item.isSelected()) {
                    item.setStatus(PartStatus.GARBAGE);
                    item.setSelected(false);
                }
            });
            drawScene();
        });
        setRemnantMenu.getItems().addAll(setAsRoll, setAsRemnant, setAsGarbage);

        MenuItem mergeSelected = new MenuItem("Merge selected");
        mergeSelected.setOnAction((ActionEvent event) -> {
            ArrayList<Part> selected = new ArrayList<>();
            remnantList.forEach(item -> {
                if (item.isSelected()) selected.add(item);
            });

            ArrayList<Part> merged = mergeParts(selected);
            remnantList.removeAll(selected);
            remnantList.addAll(merged);

            drawScene();
        });
        cm.getItems().addAll(setRemnantMenu, mergeSelected);

        remnantStackPane.setOnContextMenuRequested((ContextMenuEvent event) -> {
            cm.show(remnantStackPane, event.getSceneX(), event.getSceneY());
            event.consume();
        });

        return remnantStackPane;
    }

    private StackPane createOrderStackPane(Order order) {
        Dimension2D scaledDimensions = new Dimension2D(order.getWidth() * this.scale.getX(), order.getLength() * this.scale.getY());

        Rectangle orderRectangle = new Rectangle(scaledDimensions.getWidth(), scaledDimensions.getHeight(), PartStatus.ORDER.getColor());
        orderRectangle.setStroke(Color.BLACK);
        orderRectangle.setStrokeWidth(1);
        orderRectangle.setStrokeType(StrokeType.INSIDE);
        Label orderDimensions = new Label(String.format("(%dx%d)", order.getLength(), order.getWidth()));
        orderDimensions.setFont(Font.font("System", 8));

        StackPane orderStackPane = new StackPane(orderRectangle, orderDimensions);
        StackPane.setAlignment(orderDimensions, Pos.TOP_LEFT);
        StackPane.setMargin(orderDimensions, new Insets(5, 5, 5, 5));

        Tooltip orderToolTip = new Tooltip(String.format("%s\n(%dx%d)", order.getOrderNumber(), order.getLength(), order.getWidth()));
        Tooltip.install(orderStackPane, orderToolTip);

        ContextMenu cm = new ContextMenu();
        MenuItem editOrder = new MenuItem("Edit order");
        editOrder.setOnAction((ActionEvent event) -> {
            Stage orderDialog = new Stage();
            orderDialog.setScene(scenes.get("OrderDialog"));
            orderDialog.show();

            OrderDialogController controller = loaders.get("OrderDialog").<OrderDialogController>getController();
            controller.setRoll(roll);
        });
        cm.getItems().add(editOrder);
        cm.getItems().add(new SeparatorMenuItem());
        MenuItem cancelOrder = new MenuItem("Cancel order");
        cancelOrder.setOnAction((ActionEvent event) -> {
            setOrder(null);
            editButton.setSelected(false);
            updateOrder();
        });
        cm.getItems().add(cancelOrder);

        orderStackPane.setOnContextMenuRequested((ContextMenuEvent event) -> {
            if (!editButton.isSelected()) cm.show(orderStackPane, event.getSceneX(), event.getSceneY());
            event.consume();
        });

        return orderStackPane;
    }

    /**
     * Split the entire roll into smaller parts
     */
    private ArrayList<Part> splitRollInParts() {
        ArrayList<Part> cutParts = new ArrayList<>();

        for (Part part : partList) {
            Part original = part;
            for (Part next : partList) {
                if (next.getLength() == original.getLength()) { // Replace part by a remnant
                    Part remnant = new Part(original.getLength(), original.getWidth());
                    remnant.setStatus(PartStatus.ROLL_PART);
                    remnant.setPosition(original.getBounds().getMinX(), original.getBounds().getMinY());

                    original = remnant;
                }
                if (next.getLength() < original.getLength()) { // Divide into two new remnants
                    // Part one (upper)
                    Part firstPart = new Part(original.getLength() - next.getLength(), original.getWidth());
                    firstPart.setStatus(PartStatus.ROLL_PART);
                    firstPart.setPosition(original.getBounds().getMinX(), original.getBounds().getMinY());

                    // Part two (lower)
                    Part secondPart = new Part(next.getLength(), original.getWidth());
                    secondPart.setStatus(PartStatus.ROLL_PART);
                    secondPart.setPosition(original.getBounds().getMinX(), next.getBounds().getMinY());

                    cutParts.add(firstPart);
                    original = secondPart;
                }
            }
            cutParts.add(original);
        }

        return cutParts;
    }

    /**
     * Cut the active order out of the part (split part in multiple parts)
     *
     * @param part
     */
    private ArrayList<Part> cutOrderFromPart(Part part) {
        Bounds orderBounds = order.getBounds();
        Map<String, Point2D> orderCorners = new HashMap<>();
        orderCorners.put("TOP_LEFT", new Point2D(orderBounds.getMinX(), orderBounds.getMinY()));
        orderCorners.put("TOP_RIGTH", new Point2D(orderBounds.getMaxX(), orderBounds.getMinY()));
        orderCorners.put("BOTTOM_LEFT", new Point2D(orderBounds.getMinX(), orderBounds.getMaxY()));
        orderCorners.put("BOTTOM_RIGHT", new Point2D(orderBounds.getMaxX(), orderBounds.getMaxY()));

        ArrayList<Part> cutParts = new ArrayList<>();
        ArrayList<Part> toBeAdded = new ArrayList<>();
        ArrayList<Part> toBeRemoved = new ArrayList<>();
        cutParts.add(part);

        boolean cutPerformed = true;
        while (cutPerformed) {
            for (Part cutPart : cutParts) {
                Bounds cutPartBounds = cutPart.getBounds();
                Range<Double> widthRange = Range.between(cutPartBounds.getMinX() + 0.1, cutPartBounds.getMaxX() - 0.1);
                Range<Double> heigthRange = Range.between(cutPartBounds.getMinY() + 0.1, cutPartBounds.getMaxY() - 0.1);

                Part firstPart = null;
                Part secondPart = null;

                cutPerformed = false;
                if (widthRange.contains(orderCorners.get("TOP_LEFT").getX())) {
                    // Split horizontally at top side

                    // First part (= left)
                    firstPart = new Part((int) cutPartBounds.getHeight(), (int) (orderBounds.getMinX() - cutPartBounds.getMinX()));
                    firstPart.setPosition(cutPartBounds.getMinX(), cutPartBounds.getMinY());
                    // Second part (= right)
                    secondPart = new Part((int) cutPartBounds.getHeight(), (int) (cutPartBounds.getMaxX() - orderBounds.getMinX()));
                    secondPart.setPosition(orderBounds.getMinX(), cutPartBounds.getMinY());

                    cutPerformed = true;
                } else if (widthRange.contains(orderCorners.get("BOTTOM_RIGHT").getX())) {
                    // Split horizontally at bottom side

                    // First part (= left)
                    firstPart = new Part((int) cutPartBounds.getHeight(), (int) (orderBounds.getMaxX() - cutPartBounds.getMinX()));
                    firstPart.setPosition(cutPartBounds.getMinX(), cutPartBounds.getMinY());
                    // Second part (= right)
                    secondPart = new Part((int) cutPartBounds.getHeight(), (int) (cutPartBounds.getMaxX() - orderBounds.getMaxX()));
                    secondPart.setPosition(orderBounds.getMaxX(), cutPartBounds.getMinY());

                    cutPerformed = true;
                } else if (heigthRange.contains(orderCorners.get("TOP_LEFT").getY())) {
                    // Split vertically at left side

                    // First part (= top)
                    firstPart = new Part((int) (orderBounds.getMinY() - cutPartBounds.getMinY()), (int) cutPartBounds.getWidth());
                    firstPart.setPosition(cutPartBounds.getMinX(), cutPartBounds.getMinY());
                    // Second part (= bottom)
                    secondPart = new Part((int) (cutPartBounds.getMaxY() - orderBounds.getMinY()), (int) cutPartBounds.getWidth());
                    secondPart.setPosition(cutPartBounds.getMinX(), orderBounds.getMinY());

                    cutPerformed = true;
                } else if (heigthRange.contains(orderCorners.get("BOTTOM_RIGHT").getY())) {
                    // Split vertically at right side

                    // First part (= top)
                    firstPart = new Part((int) (orderBounds.getMaxY() - cutPartBounds.getMinY()), (int) cutPartBounds.getWidth());
                    firstPart.setPosition(cutPartBounds.getMinX(), cutPartBounds.getMinY());
                    // Second part (= bottom)
                    secondPart = new Part((int) (cutPartBounds.getMaxY() - orderBounds.getMaxY()), (int) cutPartBounds.getWidth());
                    secondPart.setPosition(cutPartBounds.getMinX(), orderBounds.getMaxY());

                    cutPerformed = true;
                }

                if (cutPerformed) {
                    firstPart.setStatus(PartStatus.ROLL_PART);
                    toBeAdded.add(firstPart);
                    secondPart.setStatus(PartStatus.ROLL_PART);
                    toBeAdded.add(secondPart);
                    toBeRemoved.add(cutPart);
                    break;
                }
            }

            cutParts.removeAll(toBeRemoved);
            cutParts.addAll(toBeAdded);
            toBeRemoved.clear();
            toBeAdded.clear();
        }

        toBeRemoved.clear();
        cutParts.forEach(cutPart -> {
            if (orderBounds.contains(cutPart.getBounds()))
                toBeRemoved.add(cutPart);
        });
        cutParts.removeAll(toBeRemoved);
        return cutParts;
    }

    /**
     * Merge the parts
     */
    private void autoMergeParts() {
        if (!remnantList.isEmpty()) {
            ArrayList<Part> toBeMerged = new ArrayList<>();
            ArrayList<Part> bestRoll = calculateBestMatchedRoll(remnantList);
            remnantList.forEach(remnant -> {
                remnant.setStatus(PartStatus.REMNANT);
                boolean partOfRoll = false;
                for (Part rollPart : bestRoll) {
                    if (rollPart.getBounds().contains(remnant.getBounds())) partOfRoll = true;
                }
                if (!partOfRoll) toBeMerged.add(remnant);
            });

            remnantList.clear();
            remnantList.addAll(bestRoll);
            remnantList.addAll(mergeParts(toBeMerged));

            drawScene();
        }
    }

    /**
     * Calculate number of rolls from parts with same status
     *
     * @param parts
     * @return
     */
    private ArrayList<ArrayList<Part>> partsToRolls(ArrayList<Part> parts) {
        ArrayList<ArrayList<Part>> rolls = new ArrayList<>();
        ArrayList<Part> parts_ = new ArrayList<>(parts);

        while (!parts_.isEmpty()) {
            ArrayList<Part> currentRoll = calculateBestMatchedRoll(parts);

            ArrayList<Part> toBeRemoved = new ArrayList<>();
            parts_.forEach(part -> {
                for (Part rollPart : currentRoll) {
                    if (rollPart.getBounds().contains(part.getBounds())) toBeRemoved.add(part);
                }
            });
            parts_.removeAll(toBeRemoved);

            // Re-locate the roll to postion (0,0)
            currentRoll.sort(Comparator.comparingInt(Part::getXpos));
            int minX = currentRoll.get(0).getXpos();
            ArrayList<Part> firstParts = new ArrayList<>();
            currentRoll.forEach(part -> {
                if (part.getXpos() == minX) firstParts.add(part);
            });

            firstParts.sort(Comparator.comparingInt(Part::getYpos));
            int deltaX = firstParts.get(0).getXpos();
            int deltaY = firstParts.get(0).getYpos();
            currentRoll.forEach(part -> part.setPosition((double) part.getXpos() - deltaX, (double) part.getYpos() - deltaY));
            rolls.add(currentRoll);
        }

        return rolls;
    }

    private ArrayList<Part> calculateBestMatchedRoll(ArrayList<Part> parts) {
        ArrayList<Integer> xvalues = new ArrayList<>();
        parts.forEach(part -> {
            if (!xvalues.contains(part.getXpos())) xvalues.add(part.getXpos());
        });
        Collections.sort(xvalues);

        Map<ArrayList<Part>, Integer> rolls = new HashMap<>();
        for (int xvalue : xvalues) {
            ArrayList<Part> toBeMerged = new ArrayList<>();
            parts.forEach(part -> {
                if (part.getXpos() == xvalue) toBeMerged.add(part);
            });

            // Calculate roll from each starting point
            mergeParts(toBeMerged).forEach(startPoint -> {
                ArrayList<Part> currentRoll = new ArrayList<>();
                currentRoll.add(startPoint);

                // Get all parts from the next xvalue
                ArrayList<Part> nextParts = new ArrayList<>();
                parts.forEach(part -> {
                    if (part.getXpos() == startPoint.getBounds().getMaxX() && part.getBounds().getMaxY() <= startPoint.getBounds().getMaxY())
                        nextParts.add(part);
                });
                nextParts.sort(Comparator.comparingInt(Part::getYpos));

                // Attach parts onto current roll until end
                while (!nextParts.isEmpty()) {
                    ArrayList<Part> mergedParts = mergeParts(nextParts);
                    if (rollValid(currentRoll, mergedParts)) {
                        mergedParts.sort(Comparator.comparingInt(Part::getYpos).reversed());
                        Part lastPartInRoll = mergedParts.get(0);
                        nextParts.clear();
                        for (Part part : parts) {
                            if (part.getXpos() == lastPartInRoll.getBounds().getMaxX() && part.getBounds().getMaxY() <= lastPartInRoll.getBounds().getMaxY())
                                nextParts.add(part);
                        }
                        currentRoll.add(lastPartInRoll);
                        nextParts.sort(Comparator.comparingInt(Part::getYpos));
                    } else nextParts.remove(nextParts.get(0));
                }

                rolls.put(currentRoll, calculateRollSurface(currentRoll));
            });
        }

        ArrayList<Part> bestMatch = Collections.max(rolls.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
        bestMatch.forEach(part -> part.setStatus(PartStatus.ROLL_PART));

        return bestMatch;
    }

    /**
     * Helper function to check if a roll is valid with (optional) extra items to be added
     *
     * @param roll
     * @param toBeAdded
     * @return
     */
    private boolean rollValid(ArrayList<Part> roll, ArrayList<Part> toBeAdded) {
        ArrayList<Part> roll_ = new ArrayList<>(roll);

        if (toBeAdded != null) {
            ArrayList<Part> toBeAdded_ = new ArrayList<>(toBeAdded);
            toBeAdded_ = mergeParts(toBeAdded_);
            roll_.addAll(toBeAdded_);
        }

        if (!partsAlignHorizontally(roll_)) return false;

        for (Part part : roll_) {
            for (Part next : roll_) {
                if (next.getXpos() > part.getXpos()) {
                    if (next.getLength() > part.getLength()) return false;
                }
            }
        }

        return true;
    }

    private ArrayList<Part> mergeParts(ArrayList<Part> parts) {
        ArrayList<Integer> xvalues = new ArrayList<>();
        parts.forEach((part -> {
            if (!xvalues.contains(part.getXpos())) xvalues.add(part.getXpos());
        }));

        ArrayList<Part> mergedParts = new ArrayList<>();

        // Merge parts vertically
        for (int xvalue : xvalues) {
            ArrayList<Part> stackedVertically = new ArrayList<>();
            for (Part part : parts) {
                if (part.getXpos() == xvalue) {
                    stackedVertically.add(part);
                }
            }
            stackedVertically.sort(Comparator.comparingInt(Part::getYpos));

            while (!stackedVertically.isEmpty()) {
                ArrayList<Part> toBeMerged = new ArrayList<>();
                toBeMerged.add(stackedVertically.get(0));
                int sumLength = stackedVertically.get(0).getLength();
                // Find all parts that are aligned vertically (ready to be merged)
                boolean end = false;
                while (!end) {
                    end = true;
                    for (Part part : stackedVertically) {
                        toBeMerged.add(part);
                        if (!partsAlignVertically(toBeMerged)) toBeMerged.remove(part);
                        else {
                            sumLength += part.getLength();
                            end = false;
                        }
                    }
                }
                toBeMerged.sort(Comparator.comparingInt(Part::getYpos));

                Part merged = new Part(sumLength, toBeMerged.get(0).getWidth());
                merged.setStatus(toBeMerged.get(0).getStatus());
                merged.setPosition(xvalue, toBeMerged.get(0).getYpos());

                mergedParts.add(merged);
                stackedVertically.removeAll(toBeMerged);
            }
        }

        return mergedParts;
    }

    /**
     * Helper function to check if list of parts align vertically
     *
     * @param parts
     * @return
     */
    private boolean partsAlignVertically(ArrayList<Part> parts) {
        int xvalue = parts.get(0).getXpos();

        // Check if all parts have the same xvalue
        for (Part part : parts) {
            if (part.getXpos() != xvalue) return false;
        }

        // Sort parts on ypos
        parts.sort(Comparator.comparingInt(Part::getYpos));

        // Check if all parts align
        Part previous = null;
        for (Part part : parts) {
            if (previous != null) {
                if (previous.getBounds().getMaxY() != part.getBounds().getMinY()) return false;
            }
            previous = part;
        }

        return true;
    }

    /**
     * Helper function to check if list of parts align horizontally
     *
     * @param parts
     * @return
     */
    private boolean partsAlignHorizontally(ArrayList<Part> parts) {
        // Sort parts on xpos
        parts.sort(Comparator.comparingInt(Part::getXpos));

        // Check if all parts align
        Part previous = null;
        for (Part part : parts) {
            if (previous != null) {
                if (previous.getBounds().getMaxX() != part.getBounds().getMinX()) return false;
            }
            previous = part;
        }

        return true;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
        this.roll = null;
        this.order = null;
        update();
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        if (order != null) {
            if (order.getWidth() > maxWidthByLength(order.getLength())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Dimensions out of bound!");
                alert.setHeaderText("Order dimensions too large for this roll.");

                alert.showAndWait();

                order = null;
            }
        }

        this.order = order;
        updateOrder();
    }

    private int calculateRollSurface(ArrayList<Part> roll) {
        int total = 0;
        for (Part part : roll) total += part.getLength() * part.getWidth();

        return total;
    }

    /**
     * Helper function to find the maximum allowed width based on length
     *
     * @param length
     * @return
     */
    private int maxWidthByLength(int length) {
        int width = 0;
        for (Part part : roll.getParts()) {
            if (part.getLength() >= length) width += part.getWidth();
        }

        return width;
    }
}

final class RollTableItem {

    private String status;
    private String number;
    private String date;
    private String location;
    private Roll roll;

    RollTableItem(Roll roll) {
        this.roll = roll;
        setStatus(roll.isRemnant());
        setNumber(roll.getRollNumber());
        setDate(roll.getDate());
        setLocation(roll.getLocation());
    }

    String getStatus() {
        return status;
    }

    void setStatus(boolean isRemnant) {
        this.status = (isRemnant) ? "remnant" : "roll";
    }

    String getNumber() {
        return number;
    }

    void setNumber(String rollNumber) {
        this.number = rollNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date.toString();
    }

    String getLocation() {
        return location;
    }

    void setLocation(String location) {
        this.location = location;
    }

    public Roll getRoll() {
        return roll;
    }

    public void setRoll(Roll roll) {
        this.roll = roll;
    }
}
