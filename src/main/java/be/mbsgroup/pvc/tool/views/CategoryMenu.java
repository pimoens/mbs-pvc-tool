/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.views;

import static be.mbsgroup.pvc.tool.Main.loaders;
import static be.mbsgroup.pvc.tool.Main.scenes;
import be.mbsgroup.pvc.tool.controllers.ArticleDialogController;
import be.mbsgroup.pvc.tool.controllers.MainController;
import be.mbsgroup.pvc.tool.jdbc.JDBCArticleDAO;
import be.mbsgroup.pvc.tool.jdbc.JDBCCategoryDAO;
import be.mbsgroup.pvc.tool.models.Article;
import be.mbsgroup.pvc.tool.models.Category;

import java.util.*;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 *
 * @author Pieter Moens
 */
public class CategoryMenu {

    @FXML
    private TreeView categoryMenu;
    @FXML
    private Label selectedItem;

    public void initialize() {
        loadCategoryMenu();
    }

    public void loadCategoryMenu() {
        Map<Integer, CategoryTreeItem> categoryById = new HashMap<>();
        Map<Integer, Integer> parents = new HashMap<>();

        JDBCCategoryDAO categorydb = JDBCCategoryDAO.getInstance();
        ArrayList<Category> categories = categorydb.selectAll();

        for (Category category : categories) {
            categoryById.put(category.getId(), new CategoryTreeItem(category));
            parents.put(category.getId(), category.getParent_id());
        }

        CategoryTreeItem root = new CategoryTreeItem();
        for (Map.Entry<Integer, CategoryTreeItem> entry : categoryById.entrySet()) {
            int key = entry.getKey();
            int parent = parents.get(key);

            if (parent == key) {
                // In case the root item points to itself.
                root = entry.getValue();
            } else {
                TreeItem<String> parentItem = categoryById.get(parent);
                if (parentItem == null) {
                    // In case the root item has no parent in the database.
                    root = entry.getValue();
                } else {
                    parentItem.getChildren().add(entry.getValue());
                    parentItem.getChildren().sort(Comparator.comparing(t->t.getValue()));
                }
            }
        }

        JDBCArticleDAO articledb = JDBCArticleDAO.getInstance();
        ArrayList<Article> articles = articledb.selectAll();

        for (Article article : articles) {
            TreeItem<String> parentItem = categoryById.get(article.getCategory().getId());
            parentItem.getChildren().add(new ArticleTreeItem(article));
            parentItem.getChildren().sort(Comparator.comparing(t->t.getValue()));
        }

        root.setExpanded(true);
        categoryMenu.setRoot(root);

        categoryMenu.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                AbstractTreeItem selected = (AbstractTreeItem) newValue;

                if (selected == null) {
                    selectedItem.setText("");
                } else if (selected.getClass() == CategoryTreeItem.class) {
                    selectedItem.setText("Category: " + selected.getObject().toString());
                    MainController controller = loaders.get("Main").<MainController>getController();
                    controller.setArticle(null);
                } else if (selected.getClass() == ArticleTreeItem.class) {
                    selectedItem.setText("Article: " + selected.getObject().toString());
                    MainController controller = loaders.get("Main").<MainController>getController();
                    controller.setArticle((Article) selected.getObject());
                } else {
                    selectedItem.setText("");
                }
        });

        categoryMenu.setCellFactory(new Callback<TreeView<String>, TreeCell<String>>() {
            @Override
            public TreeCell<String> call(TreeView<String> param) {
                return new TreeCellImpl();
            }
        });
    }

    public void addArticleItem(Article article) {
        TreeItem<String> categoryItem = findCategoryById(article.getCategory().getId());

        int index = -1;
        for (Object child : categoryItem.getChildren()) {
            if (child.getClass() == ArticleTreeItem.class) {
                AbstractTreeItem abstractChild = (AbstractTreeItem) child;
                Article childArticle = (Article) abstractChild.getObject();
                if (childArticle.getId() == article.getId()) {
                    index = categoryItem.getChildren().indexOf(child);
                    categoryItem.getChildren().remove(child);
                    break;
                }
            }
        }

        if (index >= 0) {
            categoryItem.getChildren().add(index, new ArticleTreeItem(article));
        } else {
            categoryItem.getChildren().add(new ArticleTreeItem(article));
        }
        categoryItem.getChildren().sort(Comparator.comparing(t->t.getValue()));
    }

    private CategoryTreeItem findCategoryById(int id) {
        ArrayList<CategoryTreeItem> categories = new ArrayList<>();
        addChildrenToList(categories, (CategoryTreeItem) categoryMenu.getRoot());

        for (CategoryTreeItem item : categories) {
            Category category = (Category) item.getObject();

            if (category.getId() == id) {
                return item;
            }
        }

        return null;
    }

    private void addChildrenToList(ArrayList<CategoryTreeItem> list, AbstractTreeItem item) {
        if (item.getClass() == CategoryTreeItem.class) {
            list.add((CategoryTreeItem) item);
        }

        item.getChildren().forEach(child -> addChildrenToList(list, (AbstractTreeItem) child));
    }
}

final class TreeCellImpl extends TreeCell<String> {

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            setText(getItem() == null ? "" : getItem());
            setGraphic(getTreeItem().getGraphic());
            setContextMenu(((AbstractTreeItem) getTreeItem()).getMenu());
        }
    }
}

abstract class AbstractTreeItem extends TreeItem {

    public abstract ContextMenu getMenu();

    public abstract Object getObject();

    public abstract void setObject(Object object);
}

class CategoryTreeItem extends AbstractTreeItem {

    private Category category;
    private final JDBCCategoryDAO categorydb = JDBCCategoryDAO.getInstance();

    public CategoryTreeItem() {
        this.category = null;
    }

    private AbstractTreeItem getThis() {
        return this;
    }

    public CategoryTreeItem(Category category) {
        this.category = category;
        this.setValue(category.getName());
        Image image = new Image(getClass().getResourceAsStream("/images/category.png"));
        ImageView imv = new ImageView(image);
        imv.setFitHeight(24);
        imv.setFitWidth(24);
        imv.setSmooth(true);
        imv.setCache(true);
        this.setGraphic(imv);
    }

    @Override
    public ContextMenu getMenu() {
        ContextMenu menu = new ContextMenu();

        /**
         * Rename category
         */
        MenuItem editCategory = new MenuItem("Rename category");
        editCategory.setOnAction(event -> {
                Category category = (Category) getObject();

                TextInputDialog dialog = new TextInputDialog(category.getName());
                dialog.setTitle("Edit Category");
                dialog.setHeaderText(String.format("Edit category %s", category.getName()));
                dialog.setContentText("Please enter the new name:");

                Optional<String> result = dialog.showAndWait();
                result.ifPresent(name -> category.setName(name));
                categorydb.insertOrUpdate(category);
                setObject(category);
        });
        menu.getItems().add(editCategory);

        // TODO: Add security to delete functionality.
//        /**
//         * Remove category
//         */
//        MenuItem removeCategory = new MenuItem("Remove category");
//        removeCategory.setOnAction(event ->  {
//                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//                alert.setTitle("Remove category");
//                alert.setHeaderText("Are you sure want to remove this category and all the subitems?");
//                alert.setContentText(getValue().toString());
//
//                Optional<ButtonType> option = alert.showAndWait();
//                if (option.get() != null && option.get() == ButtonType.OK) {
//                    categorydb.deleteCategory((Category) getObject());
//                    getParent().getChildren().remove(getThis());
//                }
//        });
//        menu.getItems().add(removeCategory);
        menu.getItems().add(new SeparatorMenuItem());

        /**
         * Insert category
         */
        MenuItem insertCategory = new MenuItem("Insert category");
        insertCategory.setOnAction(event -> {
                Category parent = (Category) getObject();

                TextInputDialog dialog = new TextInputDialog("Name");
                dialog.setTitle("Create Category");
                dialog.setHeaderText(String.format("Insert a category after %s", parent.getName()));
                dialog.setContentText("Please enter the name of the new category:");

                Optional<String> result = dialog.showAndWait();

                Category category = new Category();
                if (result.isPresent()) {
                    category.setName(result.get());

                    category.setParent_id(parent.getId());

                    if (categorydb.insertOrUpdate(category) != null) {
                        CategoryTreeItem categoryitem = new CategoryTreeItem(category);
                        setExpanded(true);
                        getChildren().add(categoryitem);
                    }
                }
        });
        menu.getItems().add(insertCategory);

        /**
         * Insert article
         */
        MenuItem insertArticle = new MenuItem("Insert article");
        insertArticle.setOnAction(event -> {
                Stage stage = new Stage();
                stage.setScene(scenes.get("ManageArticle"));

                ArticleDialogController controller = loaders.get("ManageArticle").<ArticleDialogController>getController();
                controller.setCategory((Category) getObject());

                stage.setResizable(false);
                stage.show();
        });
        menu.getItems().add(insertArticle);

        return menu;
    }

    @Override
    public Object getObject() {
        return this.category;
    }

    @Override
    public void setObject(Object object) {
        this.category = (Category) object;
        this.setValue(this.category.getName());
    }
}

class ArticleTreeItem extends AbstractTreeItem {

    private Article article;

    public ArticleTreeItem(Article article) {
        this.article = article;
        this.setValue(article.getCode());
        Image image = new Image(getClass().getResourceAsStream("/images/article.png"));
        ImageView imv = new ImageView(image);
        imv.setFitHeight(24);
        imv.setFitWidth(24);
        imv.setSmooth(true);
        imv.setCache(true);
        this.setGraphic(imv);
    }

    private AbstractTreeItem getThis() {
        return this;
    }

    @Override
    public ContextMenu getMenu() {
        JDBCArticleDAO articledb = JDBCArticleDAO.getInstance();

        ContextMenu menu = new ContextMenu();
        MenuItem editArticle = new MenuItem("Edit article");
        editArticle.setOnAction(event -> {
                Stage stage = new Stage();
                stage.setScene(scenes.get("ManageArticle"));

                ArticleDialogController controller = loaders.get("ManageArticle").<ArticleDialogController>getController();
                controller.setArticle((Article) getObject());

                stage.setResizable(false);
                stage.show();
        });
        menu.getItems().add(editArticle);


        // TODO: Add security to delete functionality.
//        MenuItem removeArticle = new MenuItem("Remove article");
//        removeArticle.setOnAction(event -> {
//                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//                alert.setTitle("Remove article");
//                alert.setHeaderText("Are you sure want to remove this article?");
//                alert.setContentText(getValue().toString());
//
//                Optional<ButtonType> option = alert.showAndWait();
//                if (option.get() != null && option.get() == ButtonType.OK) {
//                    articledb.deleteArticle((Article) getObject());
//                    getParent().getChildren().remove(getThis());
//                }
//        });
//        menu.getItems().add(removeArticle);

        return menu;
    }

    @Override
    public Object getObject() {
        return this.article;
    }

    @Override
    public void setObject(Object object) {
        this.article = (Article) object;
        this.setValue(this.article.getCode());
    }
}
