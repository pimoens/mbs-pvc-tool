/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Category;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface CategoryDAO {
    public ArrayList<Category> selectAll();
    public Category selectById(int id);
    public Category insertOrUpdate(Category category);
    public int deleteCategory(Category category);
}
