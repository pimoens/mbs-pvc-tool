/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Order;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface OrderDAO {
    public ArrayList<Order> selectAll();
    public Order selectById(int id);
    public Order insertOrUpdate(Order order);
}
