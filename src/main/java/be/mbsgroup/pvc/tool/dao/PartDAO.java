/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Part;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface PartDAO {

    public ArrayList<Part> selectAll();

    public Part selectById(int id);

    public ArrayList<Part> selectAllByRollId(int id);

    public Part insertOrUpdate(Part part);

    public int deletePart(Part part);
}
