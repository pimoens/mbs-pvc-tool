/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Supplier;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface SupplierDAO {
    public ArrayList<Supplier> selectAll();
    public Supplier selectById(int id);
    public Supplier selectByName(String name);
    public int insertOrUpdate(Supplier supplier);
}
