/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Article;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface ArticleDAO {
    public ArrayList<Article> selectAll();
    public Article selectById(int id);
    public Article insertOrUpdate(Article category);
    public int deleteArticle(Article article);
}
