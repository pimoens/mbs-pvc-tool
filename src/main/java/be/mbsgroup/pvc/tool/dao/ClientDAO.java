/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Client;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface ClientDAO {
    public ArrayList<Client> selectAll();
    public Client selectById(int id);
    public Client selectByName(String name);
    public int insertOrUpdate(Client client);
}