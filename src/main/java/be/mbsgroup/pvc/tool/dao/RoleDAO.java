/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Role;
import be.mbsgroup.pvc.tool.models.User;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface RoleDAO {

    public ArrayList<Role> selectAll();

    public ArrayList<User> selectAllUsersWithRole(Role role);

    public Role selectById(int id);

    public Role selectByName(String name);

    public Role insertOrUpdate(Role role);
}
