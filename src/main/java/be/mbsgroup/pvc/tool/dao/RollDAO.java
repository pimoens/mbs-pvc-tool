/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.mbsgroup.pvc.tool.dao;

import be.mbsgroup.pvc.tool.models.Roll;
import java.util.ArrayList;

/**
 *
 * @author Pieter Moens
 */
public interface RollDAO {
    public ArrayList<Roll> selectAll();
    public Roll selectById(int id);
    public ArrayList<Roll> selectAllByArticleId(int id);
    public Roll insertOrUpdate(Roll roll);
    public int deleteRoll(Roll roll);
}
